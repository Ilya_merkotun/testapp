package app.illia.testapp.di

import app.illia.domain.usecases.GetReposUseCase
import org.koin.dsl.module

val useCaseModule = module {
    single { GetReposUseCase(get()) }
}