package app.illia.testapp.di

import app.illia.testapp.scene.repositories.RepositoriesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        RepositoriesViewModel(get(), get())
    }
}