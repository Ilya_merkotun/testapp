package app.illia.testapp.core.ui

import androidx.lifecycle.ViewModel
import app.illia.testapp.core.SchedulerProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

abstract class BaseViewModel(protected val schedulerProvider: SchedulerProvider): ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    fun <T> execute(
        observable: Observable<T>,
        onSuccess: ((T?) -> Unit)? = null,
        loading: ((Boolean?) -> Unit)?,
        onError: ((Throwable) -> Unit)?
    ) {
        loading?.invoke(true)

        observable
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .doFinally {
                loading?.invoke(false)
            }
            .subscribe({
                onSuccess?.invoke(it)
            }, {
                onError?.invoke(it)
            }).addToDisposables()
    }

    private fun addDisposable(disposable: () -> Disposable) {
        compositeDisposable.add(disposable.invoke())
    }

    protected fun Disposable.addToDisposables() {
        compositeDisposable.add(this)
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}