package app.illia.testapp.core.ui.holder

import android.widget.TextView
import app.illia.testapp.R
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder

@EpoxyModelClass(layout = R.layout.view_holder_error)
abstract class ErrorEpoxyHolder : EpoxyModelWithHolder<ErrorEpoxyHolder.Holder>() {

    @EpoxyAttribute lateinit var listener: () -> Unit
    @EpoxyAttribute lateinit var title: String

    override fun bind(holder: Holder) {
        holder.titleView.text = title
        holder.view.setOnClickListener {
            if (this::listener.isInitialized) listener()
        }
    }

    class Holder : KotlinEpoxyHolder() {
        val titleView by bind<TextView>(R.id.title)
    }
}
