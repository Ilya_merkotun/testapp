package app.illia.testapp.core.common

import java.lang.ref.WeakReference

fun <T> T.weak() = WeakReference(this)