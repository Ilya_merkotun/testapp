package app.illia.testapp.core.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : Fragment(), Messageable {
    protected open val toolbarTitleRes = 0
    protected open val toolbarTitle: CharSequence? get() = getTextIfNeeded(toolbarTitleRes)

    protected open val toolbarSubtitleRes = 0
    protected open val toolbarSubtitle: CharSequence? get() = getTextIfNeeded(toolbarSubtitleRes)

    protected open val optionsMenuRes: Int = 0

    protected val supportActivity: AppCompatActivity? get() = activity as? AppCompatActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(optionsMenuRes != 0)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        if (optionsMenuRes != 0) inflater.inflate(optionsMenuRes, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val parent = parentFragment
        if (parent is HoldingToolbar) {
            supportActivity?.supportActionBar?.setDisplayShowTitleEnabled(false)
            parent.titleTextView.text = toolbarTitle
        } else {
            supportActivity?.supportActionBar?.let(::onActionBarReady)
        }
    }

    override fun onStart() {
        super.onStart()

        if (this is VisibleToolbar) {
            if (isVisibleToolbar) supportActivity?.supportActionBar?.show()
            else supportActivity?.supportActionBar?.hide()
        }

        if (isHidden) return
    }

    // region Messageable
    override fun message(text: String, duration: Int, @Messageable.Type type: Int) {
        val d = duration(duration, type)

        when (type) {
            Messageable.SNACKBAR -> Snackbar.make(view ?: return, text, d).show()
            Messageable.TOAST,
            Messageable.UNDEFINED -> Toast.makeText(context ?: return, text, d).show()
        }
    }

    // endregion
    private fun getTextIfNeeded(@StringRes resId: Int): CharSequence? =
        if (resId != 0) getText(resId) else null

    open fun onActionBarReady(bar: ActionBar) {
        bar.title = toolbarTitle
        bar.subtitle = toolbarSubtitle
    }
}