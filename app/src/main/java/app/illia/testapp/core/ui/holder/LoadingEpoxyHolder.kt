package app.illia.testapp.core.ui.holder

import app.illia.testapp.R
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder

@EpoxyModelClass(layout = R.layout.view_holder_loader)
abstract class LoadingEpoxyHolder : EpoxyModelWithHolder<LoadingEpoxyHolder.Holder>() {
    @EpoxyAttribute lateinit var listener: () -> Unit

    override fun bind(holder: Holder) {
        holder.view.setOnClickListener {
            if (this::listener.isInitialized) listener()
        }
    }

    class Holder : KotlinEpoxyHolder() {}
}