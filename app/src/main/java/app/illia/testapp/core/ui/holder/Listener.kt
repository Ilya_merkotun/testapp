package app.illia.testapp.core.ui.holder

interface Listener<T> {
    fun onClick(item: T)
}

interface LoadingListener {
    fun onLoader()
}

interface ErrorListener {
    fun onError()
}