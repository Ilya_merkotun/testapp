package app.illia.testapp

import android.app.Application
import app.illia.data.di.networkModule
import app.illia.data.di.repositoriesModule
import app.illia.testapp.core.SchedulerProvider
import app.illia.testapp.core.SchedulerProviderImpl
import app.illia.testapp.di.useCaseModule
import app.illia.testapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            androidFileProperties()
            modules(
                module { single<SchedulerProvider> { SchedulerProviderImpl() } },
                networkModule,
                repositoriesModule,
                useCaseModule,
                viewModelModule
            )
        }
    }
}