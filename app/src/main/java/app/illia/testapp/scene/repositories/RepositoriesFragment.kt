package app.illia.testapp.scene.repositories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import app.illia.domain.entities.RepoEntity
import app.illia.testapp.R
import app.illia.testapp.core.ui.BaseFragment
import app.illia.testapp.core.ui.Messageable
import app.illia.testapp.core.ui.VisibleToolbar
import app.illia.testapp.core.ui.holder.ErrorListener
import app.illia.testapp.core.ui.holder.Listener
import app.illia.testapp.core.ui.holder.LoadingListener
import app.illia.testapp.databinding.FragmentListBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class RepositoriesFragment : BaseFragment(), VisibleToolbar, Listener<RepoEntity>, LoadingListener,
    ErrorListener {

    override val isVisibleToolbar: Boolean = false

    private lateinit var binding: FragmentListBinding

    private val model: RepositoriesViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val controller = RepoController(this, this, this)
        binding.recyclerView.setController(controller)

        model.reposLD.observe(viewLifecycleOwner, Observer {
            controller.setData(it)
        })

    }

    override fun onClick(item: RepoEntity) {
        message(
            getString(R.string.format_toast_message_click, item.name), Messageable.LENGTH_SHORT,
            Messageable.SNACKBAR
        )
    }

    override fun onLoader() {
        message(
            getString(R.string.format_toast_message_click, "loader"), Messageable.LENGTH_SHORT,
            Messageable.SNACKBAR
        )
    }

    override fun onError() {
        message(
            getString(R.string.format_toast_message_click, "Error"), Messageable.LENGTH_SHORT,
            Messageable.SNACKBAR
        )
    }
}