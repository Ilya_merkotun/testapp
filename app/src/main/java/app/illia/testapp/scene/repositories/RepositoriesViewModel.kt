package app.illia.testapp.scene.repositories

import androidx.lifecycle.MutableLiveData
import app.illia.domain.entities.RepoEntity
import app.illia.domain.usecases.GetReposUseCase
import app.illia.testapp.core.ui.BaseViewModel
import app.illia.testapp.core.model.Event
import app.illia.testapp.core.SchedulerProvider

class RepositoriesViewModel(
    schedulerProvider: SchedulerProvider,
    private val getReposUseCase: GetReposUseCase
) : BaseViewModel(schedulerProvider) {

    val reposLD: MutableLiveData<Event<List<RepoEntity>>> = MutableLiveData()

    init {
        execute(getReposUseCase.getRepos(),
            onError = {
                reposLD.value = Event.error(it)
            },
            onSuccess = {
                reposLD.value = Event.success(it)
            },
            loading = {
                if (it == true) reposLD.value = Event.loading()
            }
        )
    }
}