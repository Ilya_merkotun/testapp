package app.illia.testapp.scene.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import app.illia.testapp.R
import app.illia.testapp.core.ui.BaseFragment
import app.illia.testapp.databinding.FragmentMainRootBinding

class MainRootFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_root, container, false)
    }
}