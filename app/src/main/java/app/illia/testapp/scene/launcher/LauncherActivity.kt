package app.illia.testapp.scene.launcher

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.illia.testapp.R

class LauncherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_launcher)
    }
}