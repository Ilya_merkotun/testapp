package app.illia.testapp.scene.repositories

import android.widget.TextView
import app.illia.testapp.R
import app.illia.testapp.core.ui.holder.KotlinEpoxyHolder
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder

@EpoxyModelClass(layout = R.layout.view_holder_item)
abstract class ItemEpoxyHolder : EpoxyModelWithHolder<ItemEpoxyHolder.Holder>() {

    @EpoxyAttribute lateinit var listener: () -> Unit
    @EpoxyAttribute lateinit var name: String
    @EpoxyAttribute lateinit var description: String

    override fun bind(holder: Holder) {
        holder.nameText.text = name
        holder.descriptionText.text = description
        holder.view.setOnClickListener { listener() }
    }

    class Holder : KotlinEpoxyHolder() {
        val nameText by bind<TextView>(R.id.nameText)
        val descriptionText by bind<TextView>(R.id.descriptionText)
    }
}