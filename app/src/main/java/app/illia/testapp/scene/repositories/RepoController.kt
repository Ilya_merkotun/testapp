package app.illia.testapp.scene.repositories

import app.illia.domain.entities.RepoEntity
import app.illia.testapp.core.common.Constant
import app.illia.testapp.core.common.weak
import app.illia.testapp.core.model.Event
import app.illia.testapp.core.model.Status
import app.illia.testapp.core.ui.holder.*
import com.airbnb.epoxy.TypedEpoxyController

class RepoController(
    listener: Listener<RepoEntity>? = null,
    errorListener: ErrorListener? = null,
    loadingListener: LoadingListener? = null
) : TypedEpoxyController<Event<List<RepoEntity>>>() {

    private val listenerWeak = listener.weak()
    private val errorWeak = errorListener.weak()
    private val loadingWeak = loadingListener.weak()

    override fun buildModels(data: Event<List<RepoEntity>>?) {
        when (data?.status) {
            Status.SUCCESS -> {
                data.data?.forEach {
                    itemEpoxyHolder {
                        id(it.id)
                        name(it.name)
                        description(it.description ?: Constant.EMPTY)
                        listener { listenerWeak.get()?.onClick(it) }
                    }
                }
            }
            Status.LOADING -> {
                loadingEpoxyHolder {
                    id(this.hashCode())
                    listener { loadingWeak.get()?.onLoader() }
                }
            }
            Status.ERROR, null -> {
                errorEpoxyHolder {
                    id(this.hashCode())
                    title(data?.throwable?.message.toString())
                    listener { errorWeak.get()?.onError() }
                }
            }
        }
    }
}