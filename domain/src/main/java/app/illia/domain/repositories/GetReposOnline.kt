package app.illia.domain.repositories

import app.illia.domain.entities.RepoEntity
import io.reactivex.rxjava3.core.Observable

interface GetReposOnline {
    fun getRepos(): Observable<List<RepoEntity>>
}