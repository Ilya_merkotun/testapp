package app.illia.domain.usecases

import app.illia.domain.repositories.GetReposOnline

class GetReposUseCase(
    private val getReposOnline: GetReposOnline
) {
    fun getRepos() = getReposOnline.getRepos()
}