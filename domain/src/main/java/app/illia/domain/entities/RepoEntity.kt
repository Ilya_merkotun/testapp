package app.illia.domain.entities

class RepoEntity (
    val id: String,
    val name: String,
    val fullName: String,
    val description: String? = null
)