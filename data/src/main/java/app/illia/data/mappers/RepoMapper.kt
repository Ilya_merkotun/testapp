package app.illia.data.mappers

import app.illia.data.models.Repo
import app.illia.domain.entities.RepoEntity

class RepoMapper {
    fun map(obj: Repo): RepoEntity {
        return RepoEntity(obj.id, obj.name, obj.fullName, obj.description)
    }
}