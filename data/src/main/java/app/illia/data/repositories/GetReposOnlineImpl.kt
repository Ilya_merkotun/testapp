package app.illia.data.repositories

import app.illia.data.apiservice.Api
import app.illia.data.mappers.RepoMapper
import app.illia.domain.entities.RepoEntity
import app.illia.domain.repositories.GetReposOnline
import io.reactivex.rxjava3.core.Observable

class GetReposOnlineImpl(
    private val api: Api,
    private val mapper: RepoMapper
) : GetReposOnline {
    override fun getRepos(): Observable<List<RepoEntity>> {
        return api.getRepos()
            .map {
                it.map { r ->
                    mapper.map(r)
                }
            }
    }
}