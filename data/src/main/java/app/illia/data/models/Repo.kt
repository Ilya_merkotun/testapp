package app.illia.data.models

import app.illia.data.apiservice.Api
import com.google.gson.annotations.SerializedName

class Repo(
    @SerializedName(Api.Key.ID)
    val id: String,
    @SerializedName(Api.Key.NAME)
    val name: String,
    @SerializedName(Api.Key.FULL_NAME)
    val fullName: String,
    @SerializedName(Api.Key.DESCRIPTION)
    val description: String? = null
)