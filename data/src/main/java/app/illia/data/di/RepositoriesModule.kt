package app.illia.data.di

import app.illia.data.mappers.RepoMapper
import app.illia.data.repositories.GetReposOnlineImpl
import app.illia.domain.repositories.GetReposOnline
import org.koin.dsl.module

val repositoriesModule = module {
    single<GetReposOnline> { GetReposOnlineImpl(get(), get())  }
    single { RepoMapper() }
}