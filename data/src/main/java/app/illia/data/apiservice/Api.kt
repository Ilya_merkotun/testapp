package app.illia.data.apiservice

import app.illia.data.models.Repo
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface Api {

    object Key {
        const val ID ="id"
        const val NAME ="name"
        const val FULL_NAME ="full_name"
        const val DESCRIPTION ="description"
    }

    object Path {
        const val REPO = "orgs/square/repos"
    }


    @GET(Path.REPO)
     fun getRepos(): Observable<List<Repo>>
}